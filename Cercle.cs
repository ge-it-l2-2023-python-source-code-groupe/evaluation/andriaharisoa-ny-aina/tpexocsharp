using System;
using  TpExoCsharp;

partial class Cercle{
    public double pi = 3.14;

    public double X {get;set;}
    public double Y {get;set;}
    public double R {get;set;}

    public Cercle(double x,double y,double rayon){
        this.X = X;
        this.Y = Y;
        this.R = rayon;
    }

    public void Display(){
        Console.WriteLine($"CERCLE ({X},{Y},{R})");
    
    }
    public void Perimetre(){
        double Perimetre = (2*pi*R);
        Console.WriteLine($"Le perimetre est : {Perimetre}");
    }

    public void Surface(){
        Console.WriteLine($"La surface est : {pi*(R*R)}");
    }
}
class Point{
    public double monX{get;}
    public double monY{get;}

    public Point(double monX,double monY){
        this.monX =monX;
        this.monY=monY;
    }
}
